# Terraform Workflow with GitLab Runner Shell Executor based on AWS EC2 Auto Scaling Group and creation staging infrastructure with the runner: VPC, Network Security Groups, Application Load Balancer, Elastic IPs, EFS volume, EC2 instances.

## My post on Medium about the project:

https://medium.com/@andrworld1500/terraform-workflow-with-gitlab-runner-shell-executor-based-on-aws-ec2-auto-scaling-group-and-54c6fdb148af

## About the project:

In this project we have 2 folders with Terraform configurations:
`gitlab-runner-configuration` for creation and configuration Gitlab Runner shell executor based on AWS ASG. In this configuration we use default VPC with default subnets. This infrastructure will have: ssh key pair, auto scalling group with launch template, 1 network security group, 1 IAM role with assigned policies.

`staging-infrastructure-configuration` for deploying infrastructure on AWS with Gitlab CI/CD. This infrastructure  will have: 1 VPC, 1 application loadballanser with 2 target groups, 3 network security groups, 1 EFS volume, 3 EC2 instances.


## Additional information about the staging configuration:
- Elastic IP addresses were associated to the EC2 instances;
- Teraform backend stores in already existing S3 bucket;
- VPC is configured in terraform module;
- Separation was done in terraform structure: storing outputs, variables, modules, main resources in different files;
- On the dev EC2 instance, Apache2 was installed with a preconfigured default web page;
- On prod EC2 instances, Nginx was installed with a preconfigured default web page.

## Gitlab CI/CD:
Stages `Infracost calculation`, `TFlint check`, `TFSec check` use specific tools for checking out terraform code for cost, linter issues, and security issues. Stages `Terraform validate`, `Terraform plan`, `Terraform apply`, `Terraform destroy` are used for deploying the necessary infrastructure to AWS. Stages `Terraform apply` and `Terraform destroy` are "manual" stages, and it is necessary to manually run the stages.


## Infrastructure configurations and checks:
1. We can put in our web browser the ALB DNS name on http and it redirects to 81 port. When we refresh the web page few times, we will notice that private IP address is changing.

![ALB Redirect](images/lb_diff_servers_prod.png)

2. We can login to any our prod EC2 instances with ssh, go to /mnt/efs, create some file and this file will be alailable on other prod EC2 instance.

![Prod instances](images/efs_volume.png)

3. The cost of the created infrastructure was calculated with the Infracost tool.

Gitlab runner infrastructure cost:

![Infracost Gitlab runner](images/gitlab_runner_cost.png)

Staging infrastructure cost:

![Infracost staging](images/staging_infra_cost.png)

4. The configuration graphs were generated with Inframap tool.

Gitlab runner graph:

![Gitlab runner graph](images/gitlab.png)

Staging infra graph:

![Staging infra graph](images/staging.png)