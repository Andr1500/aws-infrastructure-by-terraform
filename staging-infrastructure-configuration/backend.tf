terraform {
  backend "s3" {
    bucket = "your-created-backend"                            # bucket for terraform state file, should be exist
    key    = "staging-infrastructure-config/terraform.tfstate" # object name in the bucket to save terraform file
    region = "eu-central-1"                                    # region where the bucket is created
  }
}
