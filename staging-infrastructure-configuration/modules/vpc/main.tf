# define VPC
resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name                 = var.vpc_name
    enable_dns_support   = true
    enable_dns_hostnames = true
  }
}

# Define the subnets
resource "aws_subnet" "public" {
  for_each = {
    for idx, az in var.az : idx => {
      availability_zone = az
      cidr_block        = var.subnet_cidr_blocks[idx]
      tags = {
        Name = var.subnet_name[idx]
      }
    }
  }
  vpc_id            = aws_vpc.vpc.id
  availability_zone = each.value.availability_zone
  cidr_block        = each.value.cidr_block
  tags              = each.value.tags
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "igw-${var.vpc_name}"
  }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  tags = {
    Name = "public-route-table"
  }
}

resource "aws_route" "public_route" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gw.id
}

resource "aws_route_table_association" "public_subnet_association" {
  count          = length(aws_subnet.public)
  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public.id
}

#Variables
variable "vpc_name" {
  type        = string
  description = "Name of the VPC"
  default     = ""
}

variable "vpc_cidr_block" {
  type        = string
  description = "CIDR block of the VPC"
  default     = ""
}

variable "az" {
  type        = list(string)
  description = "List of availability zones for subnets"
  default     = []
}

variable "subnet_cidr_blocks" {
  type        = list(string)
  description = "List of CIDR blocks for subnets"
  default     = []
}

variable "subnet_name" {
  type        = list(string)
  description = "List of names for subnets"
  default     = []
}

# outputs
output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "subnet_ids" {
  value = values(aws_subnet.public)[*].id
}
