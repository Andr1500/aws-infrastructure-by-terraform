#variables
variable "region" {
  type    = string
  default = "eu-central-1"
}

#VPC vars
variable "vpc_name" {
  type    = string
  default = "VPC1"
}

variable "vpc_cidr_block" {
  type    = string
  default = "10.0.0.0/16"
}
variable "az" {
  type    = list(string)
  default = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
}

variable "subnet_cidr_blocks" {
  type    = list(string)
  default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "subnet_name" {
  type    = list(string)
  default = ["public-subnet-1a", "public-subnet-1b", "public-subnet-1c"]
}

# EC2 vars
variable "instance_type" {
  type    = string
  default = "t3.micro"
}

variable "key_name" {
  type    = string
  default = "gitlab-runner-ssh-key"
}

